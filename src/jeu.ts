let tonScore: number = 0;
let scoreAdversaire: number = 0;

const choix: string[] = ["pierre", "papier", "ciseaux"];

window.onload = function() {
    chargerChoix();
}

function chargerChoix() {
    
    const choicesDiv = document.getElementById("choix");
    if (choicesDiv) {
        choix.forEach(choice => {
            const choixElement = document.createElement("img");
            choixElement.src = choice + ".png";
            choixElement.addEventListener("click", () => selectionnerChoix(choice));
            choicesDiv.appendChild(choixElement);
        });
    } else {
        console.error("Impossible de trouver l'élément avec l'ID 'choix'.");
    }
}

function selectionnerChoix(toi: string) {
    if (tonScore < 5 && scoreAdversaire < 5) {
        const votreChoixImg = document.getElementById("votre-choix") as HTMLImageElement;
        const choixAdversaireImg = document.getElementById("choix-adversaire") as HTMLImageElement;
        if (votreChoixImg && choixAdversaireImg) {
            votreChoixImg.src = toi + ".png";
            const adversaire = choix[Math.floor(Math.random() * 3)];
            choixAdversaireImg.src = adversaire + ".png";
            if ((toi === "pierre" && adversaire === "ciseaux") ||
                (toi === "ciseaux" && adversaire === "papier") ||
                (toi === "papier" && adversaire === "pierre")) {
                tonScore += 1;
            } else {
                scoreAdversaire += 1;
            }
            document.getElementById("votre-score")!.innerText = tonScore.toString();
            document.getElementById("score-adversaire")!.innerText = scoreAdversaire.toString();
            if (tonScore === 5 || scoreAdversaire === 5) {
                const message = tonScore === 5 ? "Bien joué champion!" : "t'es mid";
                const rejouer = confirm(message + " Rejouer ?");
                if (rejouer) {
                    tonScore = 0;
                    scoreAdversaire = 0;
                    votreChoixImg.src = "";
                    choixAdversaireImg.src = "";
                    document.getElementById("votre-score")!.innerText = "0";
                    document.getElementById("score-adversaire")!.innerText = "0";
                } else {
                    tonScore = 5;
                    scoreAdversaire = 5;
                }
            }
        } else {
            console.error("Impossible de trouver les éléments nécessaires.");
        }
    } else {
        alert("La partie est terminée !");
    }
}
